package com.baglie.enemyservice.controller;

import com.baglie.enemyservice.dto.UnitStateDto;
import com.baglie.enemyservice.dto.UnitDto;
import com.baglie.enemyservice.entity.CreateSubdivision;
import com.baglie.enemyservice.entity.Subdivision;
import com.baglie.enemyservice.entity.UnitState;
import com.baglie.enemyservice.service.SubdivisionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/subdivisions")
public class SubdivisionController {

    private final SubdivisionService subdivisionService;

    @PostMapping
    public ResponseEntity<Subdivision> createSubdivision(@RequestBody Subdivision subdivision) {

        subdivisionService.createSubdivision(subdivision);

        return ResponseEntity
                .created(URI.create("/subdivisions"+ subdivision.getId()))
                .build();
    }

    @GetMapping("/{id}/enemies")
    public ResponseEntity<List<UnitDto>> getEnemiesBySubdivisionId(@PathVariable Integer id) {
        return ResponseEntity.ok(subdivisionService.getUnitsBySubdivisionId(id));
    }

    @PatchMapping("/{subdivisionId}/units/{enemyId}/add")
    public ResponseEntity<?> addEnemyToSubdivisions(@PathVariable Integer enemyId,
                                                    @PathVariable Integer subdivisionId) {
        subdivisionService.addEnemyToSubdivision(enemyId, subdivisionId);

        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/units/{enemyId}/remove")
    public ResponseEntity<?> deleteEnemyFromSubdivision(@PathVariable Integer enemyId) {
        subdivisionService.removeEnemyFromSubdivision(enemyId);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeSubdivision(@PathVariable Integer id) {
        subdivisionService.removeSubdivision(id);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/battle/{battleId}/start")
    public ResponseEntity<?> startFight(@PathVariable Integer id,
                                        @PathVariable Integer battleId) {
        subdivisionService.startFightForSubdivision(id, battleId);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/units/state/update")
    public ResponseEntity<?> updateEnemyDeadStatus(@RequestBody UnitStateDto unitStateDto) {
            subdivisionService.setEnemyDeadStatus(unitStateDto);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/create")
    public ResponseEntity<Subdivision> createSubdivision(@RequestBody CreateSubdivision createSubdivision) {
        return ResponseEntity.ok(subdivisionService.createSubdivisionWithOwnEnemies(createSubdivision));
    }

}

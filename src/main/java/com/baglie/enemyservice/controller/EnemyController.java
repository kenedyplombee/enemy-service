package com.baglie.enemyservice.controller;

import com.baglie.enemyservice.entity.Unit;
import com.baglie.enemyservice.exceptions.NotFoundException;
import com.baglie.enemyservice.service.EnemyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/enemies")
@RequiredArgsConstructor
public class EnemyController {

    private final EnemyService enemyService;

    @PostMapping
    public ResponseEntity<Unit> createEnemy(@RequestBody Unit enemy) {
        enemyService.createEnemy(enemy);
        return ResponseEntity
                .created(URI.create("enemy/"+ enemy.getId()))
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeEnemyById(@PathVariable Integer id) {
        enemyService.removeEnemyById(id);

        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<Unit>> getAllUnits() {
        return ResponseEntity.ok(enemyService.getAllUnits());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Resource doesn't exist or has been deleted")
    public void handleNotFound() { }
}

package com.baglie.enemyservice.dto;

import com.baglie.enemyservice.entity.UnitType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WinnerDto {
    private UnitType winner;
}

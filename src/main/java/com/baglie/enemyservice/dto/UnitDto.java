package com.baglie.enemyservice.dto;

import com.baglie.enemyservice.entity.UnitState;
import com.baglie.enemyservice.entity.UnitType;
import lombok.Data;

@Data
public class UnitDto {

     private Integer unitId;
     private Integer posX;
     private Integer posY;
     private Integer protectionLevel;
     private UnitType unitType;
     private UnitState unitState;
}

package com.baglie.enemyservice.dto;


import com.baglie.enemyservice.entity.UnitState;
import com.baglie.enemyservice.entity.UnitType;
import lombok.Data;

@Data
public class UnitStateDto {

    private Integer unitId;
    private UnitType unitType;
    private UnitState unitState;
}

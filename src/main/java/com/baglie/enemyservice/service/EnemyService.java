package com.baglie.enemyservice.service;

import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.dto.UnitStateDto;
import com.baglie.enemyservice.dto.WinnerDto;
import com.baglie.enemyservice.entity.Unit;
import com.baglie.enemyservice.entity.UnitState;
import com.baglie.enemyservice.entity.UnitType;
import com.baglie.enemyservice.exceptions.NotFoundException;
import com.baglie.enemyservice.repository.EnemyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.baglie.enemyservice.entity.UnitState.DEAD;

@Slf4j
@Service
@RequiredArgsConstructor
public class EnemyService {

    private final EnemyRepository enemyRepository;
    private final BattleManagerService battleManagerService;

    public Unit createEnemy(Unit enemy) {
        return enemyRepository.save(enemy);
    }

    public void removeEnemyById(Integer id) {
        enemyRepository.findById(id).orElseThrow(NotFoundException::new);
        enemyRepository.deleteById(id);
    }

    public Unit findByPosXAndPosY(Integer posX, Integer posY) {
        return enemyRepository.findByPosXAndPosY(posX, posY).orElseThrow(NotFoundException::new);
    }
    public List<Unit> findUnitsBySubdivisionId(Integer id) {
        return  enemyRepository.findUnitsBySubdivisionId(id);
    }
    public List<Unit> findUnitsBySubdivisionIdAndUnitState(Integer id, UnitState unitState) {
        return enemyRepository.findUnitsBySubdivisionIdAndUnitState(id, unitState);
    }

    public void updatePosition(PositionUpdateDto updateDto){
        battleManagerService.updateUnitPosition(updateDto);
    }

    public void updateUnitState(UnitStateDto dto){
        battleManagerService.updateUnitState(dto);
    }

    public Optional<Unit> findById(Integer id) {
        return enemyRepository.findById(id);
    }

    public Unit saveEnemy(Unit enemy){
        return enemyRepository.save(enemy);
    }

    public List<Unit> getAllUnits() {
        return enemyRepository.findAll();
    }

    public void setEnemyDeadStatus(Integer id) {

        log.info("ID for update alive status {}, ", id);

        Unit enemy = enemyRepository.findById(id).orElseThrow(NotFoundException::new);
        enemy.setUnitState(DEAD);
        enemyRepository.save(enemy);

        Unit enemy1 = enemyRepository.findById(id).orElseThrow(NotFoundException::new);
        log.info("is alive status after killed in battle {}", enemy1.getUnitState());
    }

    public void stopBattle(WinnerDto winner) {
        battleManagerService.stopBattle(winner);
    }

    public void setEnemyCriticalDistanceStatus(Integer subdivisionId){

        List<Unit> enemies = findUnitsBySubdivisionIdAndUnitState(subdivisionId,
                UnitState.ACTIVE);

        log.info("list must not be empty ----- {}", enemies.size());
        enemies.forEach(enemy -> {
            enemy.setUnitState(UnitState.CRITICAL_DISTANCE_REACHED);
            saveEnemy(enemy);
            battleManagerService.setEnemyCriticalDistanceState(enemy.getId(), enemy.getUnitType());
        });
    }
}

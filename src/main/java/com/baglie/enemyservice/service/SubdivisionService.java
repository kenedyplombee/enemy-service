package com.baglie.enemyservice.service;

import com.baglie.enemyservice.dto.UnitDto;
import com.baglie.enemyservice.dto.UnitStateDto;
import com.baglie.enemyservice.entity.*;
import com.baglie.enemyservice.exceptions.NotFoundException;
import com.baglie.enemyservice.repository.BattleManagerRepository;
import com.baglie.enemyservice.repository.SubdivisionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class SubdivisionService {

    private final SubdivisionRepository subdivisionRepository;
    private final EnemyService enemyService;
    private final BattleManagerService battleManagerService;

    public Subdivision createSubdivision(Subdivision subdivision) {
        return subdivisionRepository.save(subdivision);
    }

     public List<UnitDto> getUnitsBySubdivisionId(Integer id) {
        List<Unit> enemies = enemyService.findUnitsBySubdivisionId(id);
        List<UnitDto> unitDtoList = new LinkedList<>();

        enemies.forEach(enemy -> {
            UnitDto unitDto = new UnitDto();

            unitDto.setUnitId(enemy.getId());
            unitDto.setPosX(enemy.getPosX());
            unitDto.setPosY(enemy.getPosY());
            unitDto.setProtectionLevel(enemy.getProtectionLevel());
            unitDto.setUnitType(enemy.getUnitType());
            unitDto.setUnitState(enemy.getUnitState());

            unitDtoList.add(unitDto);
        });
        return unitDtoList;
    }

    public void addEnemyToSubdivision(Integer enemyId, Integer subdivisionId) {

        Unit maybeUnit = enemyService.findById(enemyId).orElseThrow(NotFoundException::new);
        maybeUnit.setSubdivisionId(subdivisionId);

        enemyService.saveEnemy(maybeUnit);
    }

    public void removeSubdivision(Integer subdivisionId) {
        List<Unit> units = enemyService.findUnitsBySubdivisionId(subdivisionId);
        units.forEach(enemy -> {
            enemy.setSubdivisionId(null);
            enemyService.saveEnemy(enemy);
        });

        subdivisionRepository.deleteById(subdivisionId);
    }

    public void removeEnemyFromSubdivision(Integer enemyId) {
        Unit maybeEnemy = enemyService.findById(enemyId).orElseThrow(NotFoundException::new);

            maybeEnemy.setSubdivisionId(null);
            enemyService.saveEnemy(maybeEnemy);
    }

    public void startFightForSubdivision(Integer subdivisionId, Integer battleId) {
        List<Unit> enemies =  enemyService.findUnitsBySubdivisionId(subdivisionId);
        battleManagerService.setBattleId(battleId);

        enemies.forEach(enemy -> {
            Enemy unit = new Enemy(
                    enemy.getId(),
                    enemy.getPosX(),
                    enemy.getPosY(),
                    enemy.getProtectionLevel(),
                    enemy.getUnitType(),
                    enemy.getUnitState(),
                    enemy.getSpeed(),
                    enemyService
            );
            new Thread(unit).start();
        });
    }

    public void setEnemyDeadStatus(UnitStateDto stateDto) {
        enemyService.setEnemyDeadStatus(stateDto.getUnitId());
    }

    public Subdivision createSubdivisionWithOwnEnemies(CreateSubdivision createSubdivision){
        Subdivision subdivision = subdivisionRepository.save(new Subdivision(createSubdivision.getSubdivisionName()));

        createSubdivision.getPositions().forEach(position -> {
            Unit enemy = new Unit();

            enemy.setSubdivisionId(subdivision.getId());
            enemy.setUnitState(createSubdivision.getUnitState());
            enemy.setPosX(position.getPosX());
            enemy.setPosY(position.getPosY());
            enemy.setSpeed(createSubdivision.getSpeed());
            enemy.setProtectionLevel(createSubdivision.getProtectionLevel());
            enemy.setUnitType(createSubdivision.getUnitType());

            enemyService.saveEnemy(enemy);
        });

        return subdivision;
    }
}

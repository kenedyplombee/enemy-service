package com.baglie.enemyservice.service;


import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.dto.UnitStateDto;
import com.baglie.enemyservice.dto.WinnerDto;
import com.baglie.enemyservice.entity.UnitState;
import com.baglie.enemyservice.entity.UnitType;
import com.baglie.enemyservice.repository.BattleManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BattleManagerService {

    private final BattleManagerRepository battleManagerRepository;

    private Integer battleId;

    public void setBattleId(Integer battleId) {
        this.battleId = battleId;
    }

    public void updateUnitPosition (PositionUpdateDto updateDto) {
        battleManagerRepository.updateUnitPosition(updateDto, battleId);
    }

    public void updateUnitState(UnitStateDto dto) {
        battleManagerRepository.updateEnemyCriticalDistanceStatus(dto, battleId);
    }

    public void setEnemyCriticalDistanceState(Integer unitId, UnitType unitType){
        UnitStateDto stateDto = new UnitStateDto();

        stateDto.setUnitId(unitId);
        stateDto.setUnitState(UnitState.CRITICAL_DISTANCE_REACHED);
        stateDto.setUnitType(unitType);

        battleManagerRepository.updateEnemyCriticalDistanceStatus(stateDto, battleId);
    }

    public void stopBattle(WinnerDto winner) {
        battleManagerRepository.stopBattle(battleId, winner);
    }
}

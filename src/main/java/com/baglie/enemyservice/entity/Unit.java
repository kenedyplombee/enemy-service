package com.baglie.enemyservice.entity;

import lombok.Data;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
public class Unit {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private Integer posX;
    private Integer posY;
    private Integer protectionLevel;
    private Integer speed;

    @Enumerated(STRING)
    private UnitType unitType;

    @Enumerated(STRING)
    private UnitState unitState;

    private Integer subdivisionId;

    public Unit(){}

    public Unit(Integer id, Integer posX, Integer posY, Integer protectionLevel, UnitType unitType, UnitState unitState, Integer speed) {
        this.id = id;
        this.posX = posX;
        this.posY = posY;
        this.protectionLevel = protectionLevel;
        this.unitType = unitType;
        this.unitState = unitState;
        this.speed = speed;
    }

}

package com.baglie.enemyservice.entity;

import jdk.internal.loader.AbstractClassLoaderValue;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
public class Subdivision {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;
    private String name;

    public Subdivision(){}

    public Subdivision(String name) {
        this.name = name;
    }
}

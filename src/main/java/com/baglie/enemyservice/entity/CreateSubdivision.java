package com.baglie.enemyservice.entity;

import lombok.Data;

import javax.persistence.Enumerated;

import java.util.List;

import static javax.persistence.EnumType.STRING;

@Data
public class CreateSubdivision {

    private String subdivisionName;
    private Integer protectionLevel;
    private Integer speed;

    @Enumerated(STRING)
    private UnitType unitType;

    @Enumerated(STRING)
    private UnitState unitState;

    private List<Position> positions;
}

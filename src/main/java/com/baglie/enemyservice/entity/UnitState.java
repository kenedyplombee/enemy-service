package com.baglie.enemyservice.entity;

public enum UnitState {
    ACTIVE,
    CRITICAL_DISTANCE_REACHED,
    DEAD
}

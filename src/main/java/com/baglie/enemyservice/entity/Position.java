package com.baglie.enemyservice.entity;

import lombok.Data;

@Data
public class Position {

    private Integer posX;
    private Integer posY;

}

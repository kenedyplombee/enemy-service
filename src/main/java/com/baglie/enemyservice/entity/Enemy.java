package com.baglie.enemyservice.entity;

import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.dto.UnitStateDto;
import com.baglie.enemyservice.dto.WinnerDto;
import com.baglie.enemyservice.exceptions.NotFoundException;
import com.baglie.enemyservice.exceptions.NotFoundException;
import com.baglie.enemyservice.repository.EnemyRepository;
import com.baglie.enemyservice.service.EnemyService;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

import static com.baglie.enemyservice.entity.UnitState.CRITICAL_DISTANCE_REACHED;
import static com.baglie.enemyservice.entity.UnitState.DEAD;

@Slf4j
public class Enemy extends Unit implements Runnable {

    private final EnemyService enemyService;

    public Enemy(Integer id,
                 Integer posX,
                 Integer posY,
                 Integer protectionLevel,
                 UnitType unitType,
                 UnitState unitState,
                 Integer speed,
                 EnemyService enemyService) {
        super(id, posX, posY, protectionLevel, unitType, unitState, speed);
        this.enemyService = enemyService;
    }

    protected void move() {
        log.info("Enemy {}, start moving", this);


        while (true) {

            Unit enemy = enemyService.findById(this.getId()).orElseThrow(NotFoundException::new);

            if (enemy.getUnitState().equals(CRITICAL_DISTANCE_REACHED)) {
                log.info("Enemy '{}' reached critical distance", enemy.getId());
                break;
            }

            if (enemy.getUnitState().equals(DEAD)) {
                log.info("Stop moving, unit is destroyed");
                break;
            }

            if (enemy.getPosY() <= 2) {
                saveAndUpdateState(enemy);
                enemyService.setEnemyCriticalDistanceStatus(enemy.getSubdivisionId());
                log.info("Enemy '{}' reached critical distance", enemy.getId());
                enemyService.stopBattle(new WinnerDto(enemy.getUnitType()));
                log.info("ENEMY SERVICE CALL STOP BATTLE");
                break;
            }


            makeStepAndUpdatePosition(enemy);

            try {
                TimeUnit.SECONDS.sleep(getSpeed());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveAndUpdateState(Unit enemy) {

        this.setUnitState(CRITICAL_DISTANCE_REACHED);
        enemy.setUnitState(CRITICAL_DISTANCE_REACHED);
        enemyService.saveEnemy(enemy);

        UnitStateDto dto = new UnitStateDto();

        dto.setUnitId(enemy.getId());
        dto.setUnitType(enemy.getUnitType());
        dto.setUnitState(CRITICAL_DISTANCE_REACHED);

        enemyService.updateUnitState(dto);
    }

    private void makeStepAndUpdatePosition(Unit enemy) {

        this.setPosY(this.getPosY() - 1);
        enemy.setPosY(this.getPosY());

        enemyService.saveEnemy(enemy);

        PositionUpdateDto updateDto = new PositionUpdateDto();

        updateDto.setUnitId(this.getId());
        updateDto.setUnitType(this.getUnitType());
        updateDto.setNewPosX(this.getPosX());
        updateDto.setNewPosY(this.getPosY());

        enemyService.updatePosition(updateDto);
    }

    @Override
    public void run() {
        move();
    }
}

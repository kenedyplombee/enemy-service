package com.baglie.enemyservice.repository;

import com.baglie.enemyservice.entity.Unit;
import com.baglie.enemyservice.entity.UnitState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EnemyRepository extends JpaRepository<Unit, Integer> {

    Optional<Unit> findByPosXAndPosY(Integer posX, Integer posY);

    List<Unit> findUnitsBySubdivisionId(Integer id);

    List<Unit> findUnitsBySubdivisionIdAndUnitState(Integer id, UnitState unitState);


}

package com.baglie.enemyservice.repository;

import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.dto.UnitStateDto;
import com.baglie.enemyservice.dto.WinnerDto;
import com.baglie.enemyservice.entity.UnitState;
import com.baglie.enemyservice.entity.UnitType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class BattleManagerRepository {

    private final String template;
    private final RestTemplate restTemplate;


    public BattleManagerRepository(@Value("${battle-service-server.host}") String host,
                                      @Value("${battle-service-server.port}") Integer port,
                                      RestTemplate restTemplate) {
        this.template = "http://"+ host +":"+ port;
        this.restTemplate = restTemplate;
    }

    public void updateUnitPosition(PositionUpdateDto updateDto, Integer battleId) {

        String url = template + "/battles/" + battleId + "/units/position/update";
        HttpEntity<PositionUpdateDto> request = new HttpEntity<>(updateDto);

        restTemplate.patchForObject(url, request, Void.class);
    }

    public void updateEnemyCriticalDistanceStatus(UnitStateDto stateDto, Integer battleId) {

        String url = template + "/battles/" + battleId + "/units/state/update";
        HttpEntity<UnitStateDto> request = new HttpEntity<>(stateDto);

        restTemplate.patchForObject(url, request, Void.class);
    }

    public void stopBattle(Integer battleId, WinnerDto winner) {

        String url = template + "/battles/" + battleId + "/stop";

        HttpEntity<WinnerDto> request = new HttpEntity<>(winner);

        restTemplate.postForObject(url, request, Void.class);
    }
}

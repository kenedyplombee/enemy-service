package com.baglie.enemyservice.repository;

import com.baglie.enemyservice.entity.Subdivision;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubdivisionRepository extends JpaRepository<Subdivision, Integer> {


}

create table subdivision
(
    id   serial not null
        constraint subdivision_pkey
            primary key,
    name varchar(255)
);

create table unit
(
    id               serial not null
        constraint unit_pkey
            primary key,
    posx             integer,
    posy             integer,
    protection_level integer,
    speed            integer,
    subdivision_id   integer,
    unit_type        varchar(255),
    unit_state       varchar(255)
);
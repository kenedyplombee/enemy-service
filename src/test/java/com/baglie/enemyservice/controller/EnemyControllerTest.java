package com.baglie.enemyservice.controller;

import com.baglie.enemyservice.TestUtils;
import com.baglie.enemyservice.entity.Unit;
import com.baglie.enemyservice.entity.UnitState;
import com.baglie.enemyservice.entity.UnitType;
import com.baglie.enemyservice.repository.EnemyRepository;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.regex.Matcher;

import static org.junit.Assert.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class EnemyControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    EnemyRepository enemyRepository;

    @After
    public void cleanUp() {
        enemyRepository.deleteAll();
    }

    @Test
    public void given_WhenCreateUnit_ThenSaveSuccess() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/enemies")
                .contentType("application/json")
                .content(TestUtils.fromResource("controller/enemy/create_enemy.json")))
            .andExpect(MockMvcResultMatchers.status().isCreated());

        Assertions.assertTrue(enemyRepository.findByPosXAndPosY(2,2).isPresent());
    }

    @Test
    public void given_WhenFindAll_ThenGetAllUnits() throws Exception {

        enemyRepository.save(new Unit(null,2,2,10, UnitType.TANK, UnitState.ACTIVE, 5));
        enemyRepository.save(new Unit(null,2,4,10, UnitType.TANK, UnitState.ACTIVE, 5));

        mockMvc.perform(MockMvcRequestBuilders.get("/enemies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].posY", Matchers.is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].posY", Matchers.is(4)));
    }
}
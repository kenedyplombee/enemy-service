package com.baglie.enemyservice.controller;

import com.baglie.enemyservice.TestUtils;
import com.baglie.enemyservice.entity.*;
import com.baglie.enemyservice.exceptions.NotFoundException;
import com.baglie.enemyservice.repository.EnemyRepository;
import com.baglie.enemyservice.repository.SubdivisionRepository;
import com.baglie.enemyservice.service.EnemyService;
import com.baglie.enemyservice.service.SubdivisionService;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class SubdivisionControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    SubdivisionRepository subdivisionRepository;
    @Autowired
    SubdivisionService subdivisionService;
    @Autowired
    EnemyService enemyService;
    @Autowired
    EnemyRepository enemyRepository;

    @After
    public void cleanUp() {
        enemyRepository.deleteAll();
        subdivisionRepository.deleteAll();
    }

    @Test
    public void given_WhenCreateSubdivision_ThenSaveAccess() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/subdivisions")
                .contentType("application/json")
                .content(TestUtils.fromResource("controller.subdivision/create_subdivision.json")))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        assertEquals(1, subdivisionRepository.findAll().size());
    }

    @Test
    public void given_WhenGetUnitsBySubdivision_ThenMustReturnedListEnemies() throws Exception {
        Unit unit1 = new Unit(null,2,2,10, UnitType.TANK, UnitState.ACTIVE, 5);
        Unit unit2 = new Unit(null,2,4,10, UnitType.TANK, UnitState.ACTIVE, 5);

        unit1.setSubdivisionId(1);
        unit2.setSubdivisionId(1);

        enemyService.saveEnemy(unit1);
        enemyService.saveEnemy(unit2);

        mockMvc.perform(MockMvcRequestBuilders.get("/subdivisions/1/enemies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].posY", Matchers.is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].posY", Matchers.is(4)));
    }

    @Test
    public void given_WhenAddEnemyToSubdivision_ThenSaveSuccess() throws Exception {

        int unitId = enemyService.saveEnemy(new Unit(null,2,2,10, UnitType.TANK, UnitState.ACTIVE, 5)).getId();

        int subdivisionId = subdivisionRepository.save(new Subdivision("ALPHA")).getId();

        mockMvc.perform(MockMvcRequestBuilders.patch("/subdivisions/{subId}/units/{unitId}/add", subdivisionId, unitId))
        .andExpect(MockMvcResultMatchers.status().isNoContent());

        Unit enemy = enemyService.findByPosXAndPosY(2, 2);
        assertTrue(enemy.getSubdivisionId() == subdivisionId);
    }

    @Test
    public void given_WhenRemoveUnitFromSubdivision_ThenSubdivisionIdMustBeNull() throws Exception {
        Unit unit = new Unit(null,2,2,10, UnitType.TANK, UnitState.ACTIVE, 5);
        unit.setSubdivisionId(1);

        int unitId = enemyService.saveEnemy(unit).getId();

        mockMvc.perform(MockMvcRequestBuilders.patch("/subdivisions/units/{unitId}/remove", unitId))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Unit enemy = enemyService.findByPosXAndPosY(2, 2);
        assertNull(enemy.getSubdivisionId());
    }

    @Test
    public void given_WhenRemoveSubdivision_ThenSubdivisionMustBeDeleted() throws Exception {

        int subdivisionId = subdivisionRepository.save(new Subdivision("ALPHA")).getId();
        Unit unit = new Unit(null,2,2,10, UnitType.TANK, UnitState.ACTIVE, 5);
        unit.setSubdivisionId(subdivisionId);
        enemyService.saveEnemy(unit);

        mockMvc.perform(MockMvcRequestBuilders.delete("/subdivisions/{subdivisionId}", subdivisionId))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        List<Subdivision> sub1 = subdivisionRepository.findAll();
        Unit enemy = enemyService.findByPosXAndPosY(2, 2);

        assertTrue(sub1.size() == 0);
        assertNull(enemy.getSubdivisionId());
    }

    @Test
    public void given_When_Then() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/subdivisions/create")
                .contentType("application/json")
                .content(TestUtils.fromResource("controller.subdivision/create_subdivision_with_enemy.json")))
                .andExpect(MockMvcResultMatchers.status().isOk());

        List<Unit> enemy = enemyService.getAllUnits();
        assertEquals(2, enemy.size());
        assertTrue(enemy.get(0).getPosX() == 2);


    }
}